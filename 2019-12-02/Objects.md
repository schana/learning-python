# Objects in Python

Objects are instances of classes that can have data attributes and methods that act on them.

Example:

```python
class Dog:
    def __init__(self, name):
        self.name = name

    def speak(self):
        print('woof')
    
    def get_name(self):
        print(self.name)
```

This class represents a dog. When it is intstantiated (or initialized), it is assigned a name.

```python
my_first_dog = Dog('alice')
my_second_dog = Dog('bob')
my_first_dog.speak() # prints 'woof'
my_second_dog.get_name() # prints 'bob'
```

We can use this pattern to begin to explore all sorts of fun things :)

...to be continued
