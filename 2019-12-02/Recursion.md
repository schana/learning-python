# Recursion

Funtions that call themselves

Example:

```python
def counter(i):
    if i > 0:
        print(i)
        counter(i - 1)

counter(10)
```

is functionally the same as

```python
i = 10
while i > 0:
    print(i)
    i -= 1
```

## Why would we do this?

Sometimes, it is easier to write code recursively than using loops. In some instances, breaking down the problem into smaller pieces is simpler and the logic can be followed more clearly.

That being said, any recursive function can be written using loops.

## Factorials

In math, a factorial of N is defined as the product of N * (N - 1) * (N - 2) * ... * 2 * 1, denoted as N!

```python
n = 10
factorial = 1
while n > 0:
    factorial *= n
    n -= 1
print(factorial)
```

How would you write this as a recursive function?
